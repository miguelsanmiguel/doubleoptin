require_dependency "doubleoptin/application_controller"

module Doubleoptin
  class SubscribersController < ApplicationController
    before_action :authenticate, only: [:index, :edit, :update, :destroy]
    before_action :set_subscriber, only: [:edit, :update, :destroy]

    def new
      @subscriber = Doubleoptin::Subscriber.new
    end

    def create
      @subscriber = Doubleoptin::Subscriber.new params.require(:subscriber).permit(:name, :email)

      respond_to do |format|
        if @subscriber.save
          SubscriptionMailer.confirm(@subscriber).deliver_now
          format.html
        else
          format.html { redirect_to subscription_path }
        end
      end
    end

    # POST /confirm?key=1 (not REST)
    def confirm
      @subscriber = Doubleoptin::Subscriber.find_by_nonce :confirm, params[:key]

      respond_to do |format|
        if !@subscriber.unsubscribed && @subscriber.try(:update_attribute, :confirmed, true)
          format.html
        else
          format.html { redirect_to :root }
        end
      end
    end

    # POST /unsubscribe?key=1 (not REST)
    def unsubscribe
      @subscriber = Doubleoptin::Subscriber.find_by_nonce :unsubscribe, params[:key]

      respond_to do |format|
        if @subscriber.try :update_attribute, :unsubscribed, true
          format.html
        else
          format.html { redirect_to :root }
        end
      end
    end

    def index
      @subscribers = Subscriber.all
    end

    def edit
    end

    def update
      respond_to do |format|
        if @subscriber.update params.require(:subscriber).permit(:name, :email, :unsubscribed, :confirmed)
          format.html { redirect_to subscribers_url, notice: 'Subscriber was successfully updated.' }
          format.json { render :index, status: :ok }
        else
          format.html { render :edit }
          format.json { render json: @subscriber.errors, status: :unprocessable_entity }
        end
      end
    end

    def destroy
      @subscriber.destroy
      respond_to do |format|
        format.html { redirect_to subscribers_url, notice: 'Subscriber was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    def authenticate
      super
    end

    private

    def set_subscriber
      @subscriber = Subscriber.find(params[:id])
    end

  end
end
