module Doubleoptin
  class Subscriber < ApplicationRecord
    before_validation :normalize_email, on: :create
    validates :email, format: { with: URI::MailTo::EMAIL_REGEXP }
    validates :email, presence: true, on: :create
    validates :email, uniqueness: true, on: :create, if: :all_previous_gone?
    validates :name, presence: true, on: :create

    def normalize_email
      email.downcase!
    end

    def all_previous_gone?
      ! self.class.where(email: email).not_gone.empty?
    end

    def active?
      confirmed and not unsubscribed or false
    end

    scope :not_gone, -> { where(unsubscribed: [nil, false]) }
    scope :active, -> { not_gone.where(confirmed: true) }

    def address
      "#{name} <#{email}>"
    end

    def confirm_link
      @confirm ||= digest :confirm
    end

    def unsubscribe_link
      @unsubscribe ||= digest :unsubscribe
    end

    private

    def digest(nonce)
      self.class.digest(nonce, email)
    end

    class << self
      # Return the last subscriber with the email
      def find_by_nonce(nonce, digest)
        found = all.map(&:email).find do |email|
          digest(nonce, email) == digest
        end
        self.where(email: found).last
      end

      # Return a hash digest made of three parts:
      # - a given nonce
      # - a given email adddress
      # - the number of times that the email address appears in the DB
      def digest(nonce, email)
        Digest::MD5.hexdigest("#{nonce}:#{where(email: email).size}:#{email}")
      end
    end
  end
end
