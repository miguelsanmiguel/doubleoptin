module Doubleoptin
  class SubscriptionMailer < ApplicationMailer
  layout 'mailer'

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.subscriber_subscription_mailer.confirm.subject
  #
  def confirm(subscriber)
    @subscriber = subscriber
    @confirm_link = confirm_url key: @subscriber.confirm_link
    @unsubscribe_link = unsubscribe_url key: @subscriber.unsubscribe_link

    mail to: @subscriber.address,
         subject: t('subscriber_subscription_mailer.confirm.subject')
  end
  end
end
