require 'test_helper'
require 'webdrivers'

class AdminSubscribersTest < ActionDispatch::IntegrationTest
  include Rails.application.routes.url_helpers
  include Capybara::Minitest::Assertions
  fixtures :doubleoptin_subscribers

  describe 'administrate subscribers' do
    def setup
      Doubleoptin::ApplicationController.define_method :authenticate do end
      @subscriber = doubleoptin_subscribers :cfuf
    end

    test 'forbid unauthenticated access' do
      Doubleoptin::ApplicationController.class_eval do
        def authenticate
          redirect_to root_path
        end
      end

      visit doubleoptin.subscribers_url
      assert_css 'form'
    end

    test "visiting the index" do
      visit doubleoptin.subscribers_url
      assert_selector "h1", text: "Subscribers"
    end

    test "updating a Subscriber" do
      visit doubleoptin.subscribers_url
      click_on "Edit", match: :first

      check "Confirmed" if @subscriber.confirmed
      fill_in "Email", with: @subscriber.email
      fill_in "Name", with: @subscriber.name
      check "Unsubscribed" if @subscriber.unsubscribed
      click_on "Update Subscriber"

      assert_text "Subscriber was successfully updated"
    end

    test "destroying a Subscriber" do
      Capybara.current_driver = :selenium
      visit doubleoptin.subscribers_url
      page.accept_prompt do
        click_on "Destroy", match: :first
      end

      assert_text "Subscriber was successfully destroyed"
    end

  end
end
