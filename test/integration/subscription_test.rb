require 'test_helper'

class SubscriptionTest < ActionDispatch::IntegrationTest
  include ActionMailer::TestHelper
  fixtures :doubleoptin_subscribers

  describe 'public subscription' do
    def setup
      @subscriber = Doubleoptin::Subscriber.new name: 'Alice', email: 'alice@example.com'
    end

    test 'create subscriber' do
      visit doubleoptin.subscription_path
      fill_in 'Name', with: @subscriber.name
      fill_in 'Email', with: @subscriber.email

      assert_emails 1 do
        click_button 'Subscribe'
      end

      assert page.has_content?('Thank you')
      assert page.has_content?(@subscriber.name)
    end
  end

  describe 'confirm' do
    def setup
      @subscriber = doubleoptin_subscribers :cfuf
      @confirm_link = doubleoptin.confirm_url key: @subscriber.confirm_link
    end

    test 'follow confirmation link' do
      visit @confirm_link

      assert page.has_content?('Welcome')
    end
  end

  describe 'unsubscribe' do
    def setup
      @subscriber = doubleoptin_subscribers :ctuf
      @unsubscribe_link = doubleoptin.unsubscribe_url key: @subscriber.unsubscribe_link
    end

    test 'follow unsubscription link' do
      visit @unsubscribe_link

      assert page.has_content?('Goodbye')
    end
  end

end
