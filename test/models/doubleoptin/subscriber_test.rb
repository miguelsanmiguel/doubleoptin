require 'test_helper'

module Doubleoptin
  class SubscriberTest < ActiveSupport::TestCase
    fixtures :doubleoptin_subscribers

    describe 'standard behavior' do
      def setup
        @subscriber = Doubleoptin::Subscriber.new name: 'Alice', email: 'alice@example.com'
      end

      test 'valid subscriber' do
        assert @subscriber.valid?
      end

      test 'invalid without name' do
        @subscriber.name = ''
        refute @subscriber.valid?, 'subscriber is valid without a name'
        assert_not_nil @subscriber.errors[:name], 'no validation error for name present'
      end

      test 'invalid without email' do
        @subscriber.email = ''
        refute @subscriber.valid?
        assert_not_nil @subscriber.errors[:email]
      end

      test 'invalid with wrong email' do
        @subscriber.email = 'invalid address'
        refute @subscriber.valid?
        assert_not_nil @subscriber.errors[:email]
      end

      test 'address' do
        assert_equal @subscriber.address, 'Alice <alice@example.com>'
      end

      test 'confirm_link' do
        key = Digest::MD5.hexdigest("confirm:0:alice@example.com")
        assert_equal @subscriber.confirm_link, key
      end

      test 'unsubscribe_link' do
        key = Digest::MD5.hexdigest("unsubscribe:0:alice@example.com")
        assert_equal @subscriber.unsubscribe_link, key
      end
    end

    describe 'unsubscribe' do
      def setup
        @confirmed = doubleoptin_subscribers :ctuf
        @repeated = Doubleoptin::Subscriber.new(
          name: @confirmed.name,
          email: @confirmed.email
        )
      end
      test 'forbid email repetition' do
        refute @repeated.valid?
      end
      test 'allow email repetition if unsubscribed' do
        @confirmed.update_attribute :unsubscribed, true
        assert @repeated.valid?
      end
    end

    describe 'active' do
      def setup
        @inactive = doubleoptin_subscribers :ctut, :cfuf, :cfut
        @active = doubleoptin_subscribers :ctuf
      end

      test 'list of all' do
        assert_equal Doubleoptin::Subscriber.active, [ @active ]
      end

      test 'active?' do
        assert @active.active?
        @inactive.each do |subscriber|
          refute subscriber.active?
          # Allow false but not nil
          refute subscriber.active?.nil?
        end
      end
    end

  end
end
