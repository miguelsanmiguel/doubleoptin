# Configure Rails Environment
ENV["RAILS_ENV"] = "test"

require File.expand_path('../../test/dummy/config/environment', __FILE__)
require 'rails/test_help'
require "minitest/rails/capybara"
require 'capybara/minitest'

# Hide warnings on `rake test`
$VERBOSE=nil

class ActionDispatch::IntegrationTest
  include Capybara::DSL
  include Doubleoptin::Engine.routes.url_helpers

  # Reset sessions and driver between tests
  # Use super wherever this method is redefined in your individual test classes
  def teardown
    Capybara.reset_sessions!
    Capybara.use_default_driver
  end

  def host
    'localhost:3000'
  end

  Capybara.configure do |config|
    config.app_host = 'http://localhost'
    config.server_port = 3000
  end
end

# Dummy Rails app for dev/test
require_relative "../test/dummy/config/environment"
ActiveRecord::Migrator.migrations_paths = [File.expand_path("../test/dummy/db/migrate", __dir__)]
ActiveRecord::Migrator.migrations_paths << File.expand_path('../db/migrate', __dir__)
require "rails/test_help"

# Filter out Minitest backtrace while allowing backtrace from other libraries
# to be shown.
Minitest.backtrace_filter = Minitest::BacktraceFilter.new

# Load fixtures from the engine
if ActiveSupport::TestCase.respond_to?(:fixture_path=)
  ActiveSupport::TestCase.fixture_path = File.expand_path("fixtures", __dir__)
  ActiveSupport::TestCase.set_fixture_class doubleoptin_subscribers: Doubleoptin::Subscriber
  ActionDispatch::IntegrationTest.fixture_path = ActiveSupport::TestCase.fixture_path
  ActiveSupport::TestCase.file_fixture_path = ActiveSupport::TestCase.fixture_path + "/files"
  ActiveSupport::TestCase.fixtures :all
end
