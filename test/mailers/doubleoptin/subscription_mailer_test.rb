require 'test_helper'
require 'capybara-email'

module Doubleoptin
  class SubscriptionMailerTest < ActionMailer::TestCase
    include Capybara::Email::DSL

    test 'confirm' do
      @subscriber = Doubleoptin::Subscriber.new name: 'Alice', email: 'alice@example.com'

      mail = SubscriptionMailer.confirm(@subscriber)

      # Send the mail, then test that it got queued
      assert_emails 1 do
        mail.deliver_now
      end

      assert_equal ['alice@example.com'], mail.to
      assert_equal 'Please confirm your subscription', mail.subject

      # Test the body of the sent mail contains what we expect it to
      open_email @subscriber.email
      assert current_email.has_xpath?('//a')
    end
  end
end
