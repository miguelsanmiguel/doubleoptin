Doubleoptin::Engine.routes.draw do
  get :subscription, to: 'subscribers#new'
  post :subscribe, to: 'subscribers#create'
  get :confirm, to: 'subscribers#confirm'
  get :unsubscribe, to: 'subscribers#unsubscribe'

  scope :admin do
    resources :subscribers, except: [:new, :create]
  end
  root to: 'subscribers#new'
end
