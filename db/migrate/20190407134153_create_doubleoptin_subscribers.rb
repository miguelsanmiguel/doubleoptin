class CreateDoubleoptinSubscribers < ActiveRecord::Migration[5.2]
  def change
    create_table :doubleoptin_subscribers do |t|
      t.string :name
      t.string :email
      t.boolean :confirmed, default: nil
      t.boolean :unsubscribed, default: nil

      t.timestamps
    end
  end
end
