$:.push File.expand_path("lib", __dir__)

# Maintain your gem's version:
require "doubleoptin/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |spec|
  spec.name        = "doubleoptin"
  spec.version     = Doubleoptin::VERSION
  spec.authors     = ["Miguel San Miguel"]
  spec.email       = ["development@miguelsanmiguel.com"]
  spec.homepage    = "https://gitlab.com/miguelsanmiguel/doubleoptin"
  spec.summary       = %q{Double opt-in for mailings in Ruby on Rails}
  spec.description = "Rails Engine for collecting subscribers to a mailing list"
  spec.license     = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    # spec.metadata["allowed_push_host"] = "TODO: Set to 'http://mygemserver.com'"
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  spec.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  spec.add_dependency "rails", "~> 5.2.3"
  spec.add_dependency "slim-rails"

  spec.add_development_dependency "capybara-email"
  spec.add_development_dependency "minitest-rails-capybara"
  spec.add_development_dependency "sqlite3"
  spec.add_development_dependency "webdrivers"
end
