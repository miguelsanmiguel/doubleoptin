# Doubleoptin
Doubleoptin is a Rails Engine which allows to collect subscribers to a mailing list through a [double opt-in](https://en.wikipedia.org/wiki/Opt-in_email#Confirmed_opt-in_(COI)/Double_opt-in_(DOI)) process. This is useful for instance if you want to send them newsletters yourself, instead of using some external service.

Doubleoptin does not manage the authentication of these subscribers along the site (for that use case you probably need something like [devise](https://github.com/plataformatec/devise/) with the `:confirmable` option). It doesn't help you with the Mailer either, which is pretty improved and straightforward in Rails. It just cares about gathering email addresses and sending mails with links for opting in and out of the mailing list.

## Installation
If you are running on *Rails 5.2.* (other versions are untested), add this line to your application's Gemfile:
```ruby
gem 'doubleoptin'
```

And then execute:
```bash
$ bundle
```

Once installed you need to copy the migrations for the Subscriber model with:
```bash
$ rails doubleoptin:install:migrations
```

Now you can run the migration:
```bash
$ rails db:migrate
```

Finally, you must mount Doubleoptin into your app at the `config/routes.rb` file with:
```ruby
  mount Doubleoptin::Engine, at: "/doubleoptin"
```

For other installation options see Rails' [Getting started with Engines](https://edgeguides.rubyonrails.org/engines.html#hooking-into-an-application) guide.

## Usage

### Publicly accessible subscription
Visitors of your site wanting to subscribe to your mailing list can leave a name and email address through a form. There is one provided under `/doubleoptin/subscribers/new`, but you probably want to improve on that in your own views.

Provided that you configured your Rails Mailer properly, the subscribers will then get a mail with a confirmation link that the must follow, before been able to receive further communications from your site. They will also get an unsubscription link, with which they can at any time opt out of the mailings. When they follow any of both links, they will respectively see a welcome or a goodbye page from the engine. Those contents (four views and a mail body) can and should be improved by you, replacing them under or `app/views/doubleoptin/subscription_mailer/confirm.html.erb` or `app/views/doubleoptin/subscribers/VIEW.html.erb`, where VIEW can be one of `new`, `create`, `confirm` and `unsubscribe`.

### Admin area
You can access a list of all subscribers under `/doubleoptin/admin/subscribers`. There you can modify or delete subscribers at will, *handle responsibly*!

This admin area might (and should) be secured if you have an `authenticate` method in your `ApplicationController`, whose policy applies in this area too. You can otherwise implement such a method under `app/controllers/doubleoptin/application_controller.rb` ensuring decoupling between your ApplicationController and Doubleoptin's like so:
```ruby
module Doubleoptin
  class ApplicationController < ActionController::Base
    protect_from_forgery with: :exception
    def authenticate
      # your code where
    end
  end
end
```

### Internal usage
The subscribers are programmatically accessible at the model `Doubleoptin::Subscriber`. The `active?` instance method tells whether a subscriber should receive newsletters, and the `active` class scope returns a list of all currently active subscribers (i.e. those who confirmed their address and did not unsubscribe).
Duplicate email addresses are allowed as long as the previous ones are all unsubscribed. Addresses are created in lowcase.

The subscribers can be used to address any newsletters that you may create and send through a mailer doing some job (not part of this engine). For convenience, an `address` method is provided for its usage at the emails' To: field.

### Hints & Clues
- Would you like to mount your main app root onto Doubleoptin's, given that you follow conventions, try this route:
  ```ruby
    root "doubleoptin/subscribers#new"
  ```
- Put your own crafted layout for Doubleoptin views under `app/views/layouts/doubleoptin/application.html.erb`. If you need additional ones, try putting this into `app/controllers/doubleoptin/subscribers_controller.rb`:
  ```ruby
  class Doubleoptin::SubscribersController < ApplicationController
    layout 'another', only: [:new, :create, :confirm, :unsubscribe]
  end
  ```
  There is also one `app/views/layouts/doubleoptin/mailer.html.erb` which might be of interest for you.
- If you prefer another subject for the subscription mail, modify it under `config/locales/en.yml` with this content:
  ```yaml
  en:
    subscriber_subscription_mailer:
      confirm:
        subject: 'Your subject'
  ```

## Contributing
You can freely contribute to Doubleoptin through merge requests or forks.

The TODO list is long, since this a primary version of the Engine. A not exhaustive one may includes currently:
- Adding i18n and allowing for custom content in the views and subscription email
- Allowing for a custom class name for Subscribers
- Adding some styling to the welcome and goodbye pages
- Adding some styling to the admin area
- Improving the functionality of the admin area:
  - Recording timestamps of the confirmation and unsubscription mails
  - Displaying timestamps
  - Grouping subscribers by email
  - Show some statistics
  - Paginate the index
- Hold different mailing lists in a single instance
- Refactoring
- ...

## License
The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
